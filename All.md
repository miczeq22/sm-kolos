# Systemy Multimedialne - kolokwium

### Do czego służy Binaryzacja?

**Binaryzacja** - (thresholding), proces konwersji obrazu z postaci kolorowej lub skali szarości do obrazu binarnego (zawierającego jedynie kolor biały lub czarny). Proces ten powoduje znaczną redukcję informacji i pozostawienie jedynie istotnych cech. Obraz "zbinaryzowany" zawiera dużo mniej informacji dla komputera niż ten posiadający trzy oddzielne kanały (obrazy kolorowe), przez co jest łatwiejszy do analizy - może być ona przeprowadzona w krótszym czasie. Jest stosowana we wszelakiej obróbce obrazu, np. do oddzielania tła od obiektów. W obrazie binarnym każdy element jest zapisywany za pomocą 1 bitu (czarny lub biały piksel) tj. wyznaczany jest próg podziału (threshold) i wszystkie elementy poniżej pewnego progu są zamieniane na 0 a powyżej na 255.

### Do czego stosowany jest filtr adaptacyjny?

Filtr adaptacyjny stosowany jest do oddzielenia dźwięku, który jest pożądany od zakłóceń (tło, szumy). Współczynnik filtrów adaptacyjnych są zmiennymi w czasie w celu optymalizacji zadanego kryterium.
Istnieją cztery podstawowe klasy zastosowań filtrów adaptacyjnych:
 - `Identyfikacja` - aktywna redukcja hałasu.
 - `Odwrotne modelowanie` - adaptacyjne wyrównanie kanału.
 - `Predykcja` - detekcja sygnałów.
 - `Likwidacja zakłóceń (interferencji)` - adaptacyjne odszumianie.

### Schemat filtru adaptacyjnego
![](./img/1.png)

- `x(n)` - sygnał filtrowany
- `d(n)` - sygnał odniesienia
- `e(n)` - sygnał błędu
- `y(n)` - wynik filtracji

Zasada działania filtru polega na adaptacyjnym przetworzeniu sygnału odniesienie w taki sposób, aby wytworzyć możliwe wierną kopię szumu zakłócającego, docierającego do czujnika głównego, która jest następnie odejmowana od sygnału pierwotnego.

### Trzy filtry specjalne FIR.

- **Hilberta(przesuwnik fazowy)** - w systemie GPS lub w każdym innym systemie, gdzie zachodzi jednocześnie potrzeba filtracji Hilberta i estymacji opóźnienia pomiędzy dwoma lub więcej sygnałami z odbiorników ustyuowanych w różnych punktach przestrzeni.

- **Różniczkujący** - realizuje funkcję różniczkowania (pochodną) przebiegu podanego na jego wejście. Gdy na wejście układu różniczkującego doprowadzi się przebieg prostokątny, to na jego wyjściu otrzyma się przebieg złożony z impulsów szpilkowych.

- **Interpolujący** - dokona właśnie interpolacji, czyli wyliczenia próbek pośrednich pomiędzy tymi, które mamy już dane.

### Twierdzenie o próbkowaniu

Sygnał ciągły może być ponownie odtworzony z sygnału dyskretnego, jeśli był próbkowany z częstotliwością co najmniej dwa razy większa od granicznej częstotliwości swojego widma.

### Zalety filtrów nierekursywnych FIR.

- `Stabliność` - nie wzbudzanie się.
- `Brak zniekształceń sygnału` - możliwość uzyskania liniowej charakterystyki fazowo-częstotliwośćiowej, filtry z liniową fazą opóźniają wszystkie składowe sygnału w jednakowym stopniu.
- `Prostota projektowania w porównaniu do filtrów IIR.`
- `Filtry FIR sa zawsze stabilne` - ponieważ w ich funkcji transmisji występują tylko zera, więc nie ma rekursywności mogącej spowodować niestabilność.
- W wielu zastosowaniach (przetwarzanie obrazów) skończona odpowiedź impulsowa jest bardzo pożądana.

### Czym jest współczynnik modulacji?

`Współczynnik modulacji Wm` - stosunek częstotliwości modulującej do częstotliwości nośnej.

![](./img/2.png)

Powiązany jest ze współczynnikiem głębokości modulacji, czyli stosunkiem amplitudy sygnału modulującego do amplitudy sygnału nośnego

![](./img/3.png)

### Co to jest kwantyzacja?

**Kwantyzacja** to nieodwracalne nieliniowe odwzorowanie statyczne zmniejszające dokładność danych przerz ograniczenie ich zbioru. Zbiór wartości wejściowych dzielony jest na przedziały, a każda z wartości wypadająca w określonym przedziale jest w wyniku kwantyzacji odwzorowana na jedną wartość wyjściową przypisaną temu przedziałowi - poziom reprezentacji. Proces kwantyzacji można przyrównać do "zaokrąglania" wartości do określonej skali.

![](./img/4.png)

### Rodzaje kwantyzacji

- `Skalarna` - kwantowane są niezależnie pojedyncze wartości:
    - `skalarna równomierna` - różnica między sąsiednimi poziomami decyzyjnymi jest taka sama.
    - `skalarna nierównomierna` - różnica między sąsiednimi poziomami decyzyjnymi jest różna.

- `Wektorowa` - jednocześnie kwantowanych jest kilka wartości.

### Budowa portu MIDI

![](./img/5.png)

`Pin 4` - zasialnie 5V.
`Pin 5` - sygnał MIDI.
`Pin 2` - uziemienie.
`Pin 1, Pin 3` - do wykorzystania.